{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Main where

import qualified Data.Foldable as Foldable
import Data.Functor.Foldable (cata)
import Data.Functor.Foldable.TH (makeBaseFunctor)
import Data.List (sort)
import GHC.Exts (IsList (..))
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

type Height = Integer

data Tree a = Leaf
            | Node Height (Tree a) a (Tree a)
            deriving (Show, Eq, Functor, Foldable, Traversable)

height :: Tree a -> Height
height Leaf           = 0
height (Node h _ _ _) = h

mkNode :: Tree a -> a -> Tree a -> Tree a
mkNode l c r = Node (max (height l) (height r) + 1) l c r

insert :: (Ord a) => a -> Tree a -> Tree a
insert a Leaf           = Node 1 Leaf a Leaf
insert a (Node _ l c r) = rebalance $ mkNode l' c r'
  where
    (l', r')
      | a < c     = (insert a l, r)
      | otherwise = (l, insert a r)

rebalance :: Tree a -> Tree a
rebalance Leaf                   = Leaf
rebalance t@(Node _ l c r)
  -- Right larger
  | δ < -1, Node _ rl rc rr <- r = case () of -- ugly hack to allow us to nest gaurds
      -- Left branch of Right larger, need "double rotation" (split)
    _ | δr > 0, Node _ rll rlc rlr <- rl  -> mkNode (mkNode l c rll) rlc (mkNode rlr rc rr)
      | otherwise                         -> mkNode (mkNode l c rl) rc rr
      where
        δr = height rl - height rr
  -- Left larger
  | δ > 1,  Node _ ll lc lr <- l = case () of -- ugly hack to allow us to nest gaurds
      -- Right branch of Left larger, need "double rotation" (split)
    _ | δl < 0, Node _ lrl lrc lrr <- lr  -> mkNode (mkNode ll lc lrl) lrc (mkNode lrr c r)
      | otherwise                         -> mkNode ll lc (mkNode lr c r)
      where
        δl = height ll - height lr
  | otherwise                    = t
  where
    δ = height l - height r

--
-- Test stuff
--

$(makeBaseFunctor ''Tree)

instance (Ord a) => IsList (Tree a) where
    type Item (Tree a) = a
    fromList = foldr insert Leaf
    toList   = Foldable.toList

balanced :: Tree a -> Bool
balanced = snd . cata go
  where
    go LeafF                       = (0, True)
    go (NodeF h (l, lb) _ (r, rb)) = (h, lb && rb && abs (l - r) <= 1)

calcHeight :: Tree a -> Height
calcHeight = cata go
  where
    go LeafF           = 0
    go (NodeF _ l _ r) = max l r + 1

genTree :: (MonadGen m, Ord a) => m a -> m (Tree a)
genTree a = Gen.recursive Gen.choice
    [ pure Leaf ]
    [ insert <$> a <*> genTree a ]

prop_goodHeight :: Property
prop_goodHeight = property $ do
    t <- forAll $ genTree Gen.alpha
    height t === calcHeight t

prop_alwaysBalanced :: Property
prop_alwaysBalanced = withTests 1000 . property $ do
    t <- forAll $ genTree Gen.alpha
    assert $ balanced t

-- Same as prop_alwaysBalanced, but will show list of inserts that cause failure
prop_alwaysBalancedFromList :: Property
prop_alwaysBalancedFromList = withTests 1000 . property $ do
    as <- forAll $ Gen.list (Range.linear 0 100) Gen.alpha
    assert $ balanced (fromList as)

prop_keepsEverything :: Property
prop_keepsEverything = property $ do
    as <- forAll $ Gen.list (Range.linear 0 100) Gen.alpha
    toList (fromList as :: Tree Char) === sort as

{-# ANN tests "HLint: ignore Redundant bracket" #-} -- HLint doesn't know about $$
tests :: IO Bool
tests = checkParallel $$(discover)

main :: IO ()
main = () <$ tests
