btree
=====

```bash
git clone https://gitlab.com/rimmington/btree
cd btree
stack test
# Smile, Smile, Smile!
```

The tree is balanced thanks to the magic of AVL tree [single](https://en.wikipedia.org/wiki/File:AVL-simple-left_K.svg) and [double](https://en.wikipedia.org/wiki/File:AVL-double-rl_K.svg) rotations.
